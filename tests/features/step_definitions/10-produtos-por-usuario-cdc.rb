Quando('faco a requisicao do produtos por usuario cdc') do
    produtosporusuario = ProdutosPorUsuarioCdc.new
    @produtos_por_usuario = produtosporusuario.listar_produtos_por_usuario_cdc($autenticacao)
end

Entao('valido o retorno do produtos por usuario cdc') do
    binding.pry
    expect(@produtos_por_usuario.code).to eq(200)
    @produtos_por_usuario.each { |row| expect(!row['descricao'].nil?).to be_truthy }
    @produtos_por_usuario.each { |row| expect(!row['rendaMinima'].nil?).to be_truthy }
    @produtos_por_usuario.each { |row| expect(!row['rendaMaxima'].nil?).to be_truthy }
end