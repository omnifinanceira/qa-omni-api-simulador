Quando('faco a requisicao do uf') do
    uf = Uf.new
    @uf = uf.listar_uf($autenticacao)
end

Entao('valido o retorno do uf') do
    expect(@uf.code).to eq(200)
    @uf['ufs'].each { |row| expect(!row['codigo'].nil?).to be_truthy }
    @uf['ufs'].each { |row| expect(!row['descricao'].nil?).to be_truthy }
    @uf['ufs'].each { |row| expect(!row['uf'].nil?).to be_truthy }
end