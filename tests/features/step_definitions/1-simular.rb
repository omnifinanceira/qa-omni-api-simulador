Quando('faco a requisicao no simulador') do
    sim = Simular.new
    @response_simulador = sim.fazer_simulacao("","",$autenticacao)
end

Entao('valido o retorno do simulador') do
    @response_simulador.code == 200
end