Quando('faco a requisicao do classe profissional') do
    seg = ClasseProfissional.new
    @lista_cp = seg.listar_classe_profissional($autenticacao)
end

Entao('valido o retorno do classe profissional') do
    expect(@lista_cp.code).to eq(200)
    @lista_cp.each { |row| expect(!row['id'].nil?).to be_truthy }
    @lista_cp.each { |row| expect(!row['descricao'].nil?).to be_truthy }
end