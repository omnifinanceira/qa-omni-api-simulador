Quando('faco a requisicao do tipos transportes') do
    tipotransportes = TipoTransportes.new
    @tipo_transportes = tipotransportes.listar_tipos_transportes($autenticacao)
end

Entao('valido o retorno do tipos transportes') do
    expect(@tipo_transportes.code).to eq(200)
    @tipo_transportes['transportes'].each { |row| expect(!row['id'].nil?).to be_truthy }
    @tipo_transportes['transportes'].each { |row| expect(!row['descricao'].nil?).to be_truthy }
    @tipo_transportes['transportes'].each { |row| expect(!row['edicao'].nil?).to be_truthy }
end