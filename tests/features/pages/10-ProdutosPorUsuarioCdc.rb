class ProdutosPorUsuarioCdc < Request

    def listar_produtos_por_usuario_cdc(autenticacao)

        produtosporusuariocdc = exec_get("/operacoes/produtos-por-usuario/CDC",autenticacao,"https://dev-geral.omni.com.br/api")

        return produtosporusuariocdc
    end
end