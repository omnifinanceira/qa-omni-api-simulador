class TempoPossuiCaminhao < Request

    def tempo_possui_caminhao(autenticacao)

        tempo_caminhao = exec_get("/atributo-temporal?atributo=TEMPO_POSSUI_CAMINHAO",autenticacao,"https://dev-geral.omni.com.br/api")

        return tempo_caminhao
    end
end